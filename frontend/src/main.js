// this is run with npm run serve
import Vue from 'vue'
import App from './App.vue'
import router from './router' // added for routing

Vue.config.productionTip = false

new Vue({
  router, // added for routing
  render: h => h(App),
}).$mount('#app')
