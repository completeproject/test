import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/testapi.vue'
import Websock from './components/echowebsock.vue'
import HelloWorld from './components/HelloWorld.vue'
import ShowTable from './components/showtable.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/websock', // with a slash you get the page in the url, otherwise just a name
      name: 'websock',
      component: Websock,
    },
    {
      path: '/helloworld',
      name: 'helloworld',
      component: HelloWorld,
    },
    {
      path: '/showtable',
      name: 'showtable',
      component: ShowTable,
    },
  ]
})