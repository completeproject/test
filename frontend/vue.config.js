// vue.config.js

/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */

var fs = require('fs');
const path = require('path')

//module.exports = {
// enable the following to get https
// devServer : {
//     https : {
//        key: fs.readFileSync(path.join('D:\\Stuff\\Projects\\nodeCompleteFrontBack', 'server.key')),
//        cert: fs.readFileSync(path.join('D:\\Stuff\\Projects\\nodeCompleteFrontBack', 'server.cert'))
//     }
// }  
//}

// requires restart to read these options
module.exports = {
    devServer: {
        disableHostCheck: true, // this disables browser side pinging of e.g. 10.15.0.5:8080/sockjs-node/info?t=1614788304384
                                // still does it once at the beginning
//        port: 4000,
//        public: '0.0.0.0:4000'
    },
    publicPath: "/"
}
