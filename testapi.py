import requests
import json
import pandas as pd

def jprint(obj):
    # create a formatted string of the Python JSON object
    text = json.dumps(obj, sort_keys=True, indent=4)
    print(text)

thehead = {"Authorization":"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJiYXIiLCJ3aGF0ZXZlciI6InRlc3QiLCJpYXQiOjE2MTYwODE3OTl9.33DcMysf7g2JA4BqNhbGiiu8PgFXIoVMshXIZxp7PIQ"}

# get all the cars
response = requests.get("http://localhost:3000/api/cars", headers = thehead)

print(response.status_code)

#print(response.json())
jprint(response.json())

j = response.json()
df = pd.DataFrame.from_dict(j)
df.to_csv("testapi_output.csv")

# get a single car
theparameter = "605376ebca73f93b80189311"
response = requests.get("http://localhost:3000/api/cars/" + theparameter, headers = thehead)

jprint(response.json())
