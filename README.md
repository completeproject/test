# this

This is a node.js based back end and front end example.

You should be able to go into the backend and frontend directories and run npm install to get all the packages.

You should have an open Mongodb instance running on your machine.

After that you can use:

    node server.js
    or
    nodemon server.js

to start the backend.

And then you can use:

    npm run serve

to start the frontend

## Backend
* **fastify:** the framework
* **jwt:** Auth tokens
* **websockets:** Standard
* **swagger:** API human frontend
* **faker.js:** to generate some data

## Front End
* **vue:** the framework
* **vue-router:** for separate pages

[This is a link to the project](https://gitlab.com/completeproject/test).

## Example

```
git clone https://gitlab.com/completeproject/test.git
etc.
```

## License

The code is [MIT licensed](./LICENSE).