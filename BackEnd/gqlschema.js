// Import External Dependancies
const graphql = require('graphql')

// Destructure GraphQL functions
const {
	GraphQLSchema,
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLID,
	GraphQLList,
	GraphQLNonNull
} = graphql

// Import Controllers
//const carController = require('../controllers/carController')
//const ownerController = require('../controllers/ownerController')
//const serviceController = require('../controllers/serviceController')
const mongoController = require('./mongocontroller')

// Define Object Types
const carType = new GraphQLObjectType({
	name: 'Car',
	fields: () => ({
        _id: { type: GraphQLID },
		title: { type: GraphQLString },
		brand: { type: GraphQLString },
		price: { type: GraphQLString },
		age: { type: GraphQLInt }
    })
})

// Define Root Query
const RootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	fields: {
		car: {
            type: carType,
			args: { _id: { type: GraphQLID } },
			async resolve(parent, args) {
				return await mongoController.getSingleCar(args)
			}
        },
		cars: {
			type: new GraphQLList(carType),
			async resolve(parent, args) {
				return await mongoController.getCars()
			}
		}
	}
})

// Define Mutations
const Mutations = new GraphQLObjectType({
	name: 'Mutations',
	fields: {
		addCar: {
			type: carType,
			args: {},
			async resolve(args) {
				return ''
			}
		},
		updateCar: {
			type: carType,
			args: {},
			async resolve(args) {
				return ''
			}
		},
		deleteCar: {
			type: carType,
			args: {},
			async resolve(args) {
				return ''
			}
		}
	}
})

// Export the schema
module.exports = new GraphQLSchema({
	query: RootQuery,
//	mutation: Mutations
})