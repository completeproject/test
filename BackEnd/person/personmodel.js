// External Dependancies
const mongoose = require('mongoose')

// added by me
const personSchema = new mongoose.Schema({
    personName: String,
    personIP: String,
    lastModified: Date,
    aNumber: Number
  }, { collection: 'userinfo' }) // if you don't pass this Mongoose pluralizes the person to people as the schema name. MAGIC!
  
  module.exports = mongoose.model('Person', personSchema)