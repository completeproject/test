// External Dependancies
const boom = require('boom') // apparently gives html error messages

// Get Data Models
const Person = require('../person/personmodel')

///////////////////////////// PERSON Stuff here
// Get single person by ID
exports.getSinglePerson = async (req, reply) => {
    try {
      const id = req.params.id
      const person = await Person.findById(id)
      return person
    } catch (err) {
      throw boom.boomify(err)
    }
  }
  // Add a new person
  exports.addPerson = async (req, reply) => {
    try {
      const person = new Person(req.body)
      return person.save()
    } catch (err) {
      throw boom.boomify(err)
    }
  }