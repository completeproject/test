// == MAIN FILE ==
// this is the file that is started with e.g.: nodemon server.js

// required by https addition
const fs = require('fs')
const path = require('path')

// Require the framework and instantiate it
const fastify = require('fastify')({
    logger: true,
    trustProxy: true, // enables client side ip display, https://www.fastify.io/docs/latest/Server/#factory-trust-proxy
    // https enabled after running openssl to create key and cert files
//    https: {
//        key: fs.readFileSync(path.join('D:\\Stuff\\Projects\\nodeCompleteFrontBack', 'server.key')),
//        cert: fs.readFileSync(path.join('D:\\Stuff\\Projects\\nodeCompleteFrontBack', 'server.cert'))
//      }
})

// GraphQL: fastify version added like this
const gql = require('fastify-gql')
// GraphQL: Import GraphQL Schema
const schema = require('./gqlschema')
// GraphQL: Register Fastify GraphQL
fastify.register(gql, {
   schema,
   graphiql: true // goes to http://localhost:3000/graphiql
})

// JWT: added after installing fastify-jwt for token passing
fastify.register(require('fastify-jwt'), {
    secret: 'ThisIsMySecretAndNobodyKnowsIt'
})
// JWT: add a route to enable singup via a post request and send back a token
fastify.post('/signup', (req, reply) => {
    const token = fastify.jwt.sign({
        foo: 'bar', 
        whatever: 'test'
    }) // also contains iat: ??? (issued at)
    reply.send({ token })
})
// JWT: this adds a secret route that can be accessed via a get request
//      and sending (key, value) in headers: Authorization, Bearer xxxxxx
//      xxxxxx was received with the signup post request and was labeled token
fastify.decorate("authenticate", async (request, reply) => {
    try {
      await request.jwtVerify()
    } catch (err) {
      reply.send(err)
    }
  })
  .after(() => {
    fastify.route({
      method: 'GET',
      url: '/secret',
      preHandler: [fastify.authenticate],
      handler: (req, reply) => {
        reply.send('secret')
      }
    })
})

// CORS: no idea if this will link to socket.io
fastify.register(require('fastify-cors'), { 
    // put your options here
    // and if https is enabled with a self signed certificate, neither of these will help anyway, will error with CORS did not succeed
    origin : true, // without this socket returns: Reason: Credential is not supported if the CORS header ‘Access-Control-Allow-Origin’ is ‘*’
    credentials: true // without this socket returns: Reason: expected ‘true’ in CORS header ‘Access-Control-Allow-Credentials’
  })

// ws: adds websockets
fastify.register(require('fastify-websocket'))
fastify.get('/websocket/', { websocket: true }, (connection /* SocketStream */, req /* FastifyRequest */) => {
    console.log("Connected maybe ")
    connection.socket.send('Welcome')
    //console.log("number of clients " + fastify.webSocketServer.clients.size)

    const socketID = req.headers["sec-websocket-key"]
    fastify.log.info({
        msg: "Client connected",
        socket: socketID
      });


    connection.socket.on('message', message => {
        console.log("something more " + message)
      // message === 'hi from client'
      connection.socket.send('hi from server ' + message)
      fastify.websocketServer.clients.forEach(function each(client) {
        if (client.readyState === 1) {
          client.send(socketID + ' said: ' + message)
        }
    })
    })

    connection.socket.on('close', () => {
        console.log("Disconnected")
        //connection.socket.send('Bye bye')
        fastify.websocketServer.clients.forEach(function each(client) {
            if (client.readyState === 1) {
              client.send('this guy left: ' + socketID)
            }
        })
    })

    // this works
    // the ideas for the code below and above are from:
    // https://github.com/fastify/fastify-websocket/issues/45
    fastify.websocketServer.clients.forEach(client => {
        console.log("message all clients")
        client.send('someone connected, id: ' + socketID)
    })

    // so does this, it is better because it doesn't send to the one connecting
    fastify.websocketServer.clients.forEach(function each(client) {
        if (client.readyState === 1) {
          client.send('test, new guy ' + socketID)
        }
    })
    
    setInterval(() => {
        fastify.websocketServer.clients.forEach(client => {
                console.log("regular message " + client.readyState)
                client.send('tick tock')
        })
      }, 5000)
  })



// Declare a default route for the root of the API
fastify.get('/', async (request, reply) => {
    return { hello: 'world' }
})

// Declare a route to deliver header information
// https://www.fastify.io/docs/latest/Request/
fastify.get('/ipinfo', async (request, reply) => {
  return {
    headers : request.headers,
    ip : request.ip,
    ips : request.ips,
    hostname : request.hostname,
    url : request.url
  }
})



// mongodb: Require external modules to connect to mongodb
const mongoose = require('mongoose')

// mongodb: Connect to mongodb
mongoose.connect('mongodb://localhost:27017/tutorial_db')
 .then(() => console.log('MongoDB connected…'))
 .catch(err => console.log(err))

// Swagger: order matters, swagger stuff needs to come before routes
// Swagger: Import Swagger Options
const swagger = require('./swagger') // the config file which tells swagger to look a certain way
// Swagger: Register Swagger
fastify.register(require('fastify-swagger'), swagger.options)

// this function is used in routes.js to send a message after a API get that somebody got data
informdudes = async () => {
    fastify.websocketServer.clients.forEach(function each(client) {
      console.log("someone triggered get")
      if (client.readyState === 1) {
        client.send('somebody got data')
      }
  })
  }

// only add routes after swagger, otherwise it will complain and not show anything
// add the rest of the routes from the other file
const routes = require('./routes') // the api access point definitions
// loop through all the routes to add them
routes.forEach((route, index) => {
    fastify.route(route)
})

// Run the server!
const start = async () => {
  try {
    await fastify.listen(3000)
    fastify.swagger() // Swagger: do swagger stuff

    fastify.log.info(`server listening on ${fastify.server.address().port}`)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()