// Import our Controllers
const carController = require('./mongocontroller')
// added by me
const personController = require('./person/personcontroller')

const documentation = require('./routesSchema')

thisauth = async (request, reply) => {
  try {
    await request.jwtVerify()
    console.log(request.user)
  } catch (err) {
    reply.send(err)
  }
}

// defines the access points and which functions will handle the request
const routes = [
  {
    method: 'GET',
    url: '/api/cars',
    handler: carController.getCars,
    preValidation: [thisauth], // adds the authorization pre-step as per function above, also could use preHandler (something about no body)
    onRequest: [informdudes] // this function is defined in server.js and is triggered on the GET and sends websocket messages to everyone
  },
  {
    method: 'GET',
    url: '/api/cars/:_id', // basically this _id part needs to be the same name in the mongocontroller file
    handler: carController.getSingleCar,
    schema: documentation.getOneCarSchema, // from the routesSchema.js brings in the descriptions and parameters for Swagger
    preValidation: [thisauth]
  },
  {
    method: 'POST',
    url: '/api/cars',
    handler: carController.addCar,
    schema: documentation.addCarSchema,
    preValidation: [thisauth]
  },
  {
    method: 'PUT',
    url: '/api/cars/:_id',
    handler: carController.updateCar,
    preValidation: [thisauth]
  },
  {
    method: 'DELETE',
    url: '/api/cars/:_id',
    handler: carController.deleteCar,
    preValidation: [thisauth]
  },
  // the routes below deal with a person
  {
    method: 'GET',
    url: '/api/person/:id',
    handler: personController.getSinglePerson,
  },
  {
    method: 'POST',
    url: '/api/person',
    handler: personController.addPerson
  }
]

module.exports = routes