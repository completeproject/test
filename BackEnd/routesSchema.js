// this file enables swagger to pick up correct things to display in a section
// included via routes.js in schema: part
exports.addCarSchema = {
    description: 'Create a new car',
    tags: ['default'],
    summary: 'Creates new car with given values',
    body: {
      type: 'object',
      properties: {
        title: { type: 'string' },
        brand: { type: 'string' },
        price: { type: 'string' },
        age: { type: 'number' },
        services: { type: 'object' }
      }
    },
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          _id: { type: 'string' },
          title: { type: 'string' },
          brand: { type: 'string' },
          price: { type: 'string' },
          age: { type: 'number' },
          services: { type: 'object' },
          __v: { type: 'number' }
        }
      }
    }
  }

// added by me
exports.getOneCarSchema = {
    description: 'Get a single car',
    tags: ['default'],
    summary: 'Gets a single car based on its id',
    params: {
      type: 'object',
      properties: {
        _id: {
          type: 'string',
          description: 'car id'
        }
      }
    }
}