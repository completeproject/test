// this file basically tells swagger to look a certain way and where to exist
exports.options = {
    routePrefix: '/documentation',
    exposeRoute: true,
    swagger: {
      info: {
        title: 'Fastify API',
        description: 'Building a blazing fast REST API with Node.js, MongoDB, Fastify and Swagger',
        version: '1.0.0'
      },
      externalDocs: {
        url: 'https://swagger.io',
        description: 'Find more info here'
      },
      host: 'localhost:3000',
      // this next one should hopefully add the right header for all of the endpoints
      // it is a weird hack to get swagger to show Authorize selection and then send it correctly
      // basically the user needs to add the full "Bearer <token>" string in the box, i.e.:
      // Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJmb28iOiJiYXIiLCJpYXQiOjE2MTQ2MTIxNzR9.Yq0mQvTZLEfBCQNehEra817T0ZPeAgNxCRGHhqwEyNI
      // this is the thing created by JWT signup
      securityDefinitions : {
          bearerAuth : {
            type : "apiKey",
            in : "header",
            name : "Authorization"
          }
      },
      // and this one adds the bearerAuth header to every single request "at the root level"
      security : [{bearerAuth : []}],
      // changed this from http to https after enabling it
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json']
    },
//    uiConfig: {
//        docExpansion: 'full', // this will by default open up the swagger access nodes
//        deepLinking: false
//    }
  }