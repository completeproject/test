# backend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
nodemon server.js
```

### fake data
```
node seed.js
```